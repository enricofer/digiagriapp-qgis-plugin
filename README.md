This plugin is able to interfaces with [DigiAgriApp](https://gitlab.com/digiagriapp/) trough its API. You can upload and download vector and raster data, create plants from CSV file.

## How to use it

First step you need to have the user access to a [DigiAgriApp server](https://gitlab.com/digiagriapp/digiagriapp-server) instance.

Second you need to set up your plugin, set `Server`, `Username`, `Password`, click twice on `Save` button.
At this point select the farm e save again, at this point you can `Close` the settings.

Now you can use all the plugin functionalities.
