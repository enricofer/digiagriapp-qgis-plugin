# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'digiagriapp_dialog_about.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


import os
import sys
from qgis.PyQt import uic
from qgis.PyQt import QtWidgets

sys.path.append(os.path.dirname(__file__))

# This loads your .ui file so that PyQt can populate your plugin with
# the elements from Qt Designer
FORM_CLASS, _ = uic.loadUiType(
    os.path.join(os.path.dirname(__file__), "digiagriapp_dialog_about.ui"),
    resource_suffix=''
)


class DigiAgriAppAbout(QtWidgets.QDialog, FORM_CLASS):
    """Set up the user interface"""

    def __init__(self, parent=None):
        """Constructor."""
        super(DigiAgriAppAbout, self).__init__(parent)
        # Set up the user interface from Designer through FORM_CLASS.
        # After self.setupUi() you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)
