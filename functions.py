# from pyproj import CRS
# from pyproj.aoi import AreaOfInterest
# from pyproj.database import query_utm_crs_info

from qgis.PyQt.QtWidgets import QDialog
from qgis.PyQt.QtWidgets import QDialogButtonBox
from qgis.PyQt.QtWidgets import QLabel
from qgis.PyQt.QtWidgets import QVBoxLayout
from qgis.PyQt.QtWidgets import QProgressDialog
from qgis.PyQt.QtWidgets import QProgressBar

from qgis.core import QgsFeature

class QuestionDialog(QDialog):
    """Dialog with question with button box"""
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Upload data!")

        QBtn = QDialogButtonBox.Ok | QDialogButtonBox.Cancel

        self.buttonBox = QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.layout = QVBoxLayout()
        message = QLabel("Do you want to upload the last create plants?")
        self.layout.addWidget(message)
        self.layout.addWidget(self.buttonBox)
        self.setLayout(self.layout)

    # def accept(self):
    #     """Upload create data
    #     """


    # def reject(self):
    #     """Not upload data"""


class ProgressBarDialog(QProgressDialog):
    """Dialog with progress bar"""
    def __init__(self, parent=None, label=None):
        super().__init__(parent)

        self.setWindowTitle("DigiAgriApp operation progress")
        if label:
            self.setLabelText(label)
        self.setMinimumWidth(300)

        self.bar = QProgressBar(self)
        self.bar.setTextVisible(True)
        self.setBar(self.bar)
        self.bar.setValue(0)
        self.bar.setMaximum(100)


def get_utm_srid_from_extent(extent):
    """Return the right UTM code from Django geometry envelope

    Args:
        extent (obj): list or Envelope geometry in longitude and latitude (EPSG 4326)

    Returns:
        CRS object
    """
    lon = (extent[0] + extent[2]) / 2
    lat = (extent[1] + extent[3]) / 2
    EPSG_srid = int(32700-round((45+lat)/90,0)*100 + round((183+lon)/6,0))
    return EPSG_srid

# def utm_from_extent(extent):
#     """Return the right UTM code from Django geometry envelope

#     Args:
#         extent (obj): list or Envelope geometry in longitude and latitude (EPSG 4326)

#     Returns:
#         CRS object
#     """
#     utm_crs_list = query_utm_crs_info(
#         datum_name="WGS 84",
#         area_of_interest=AreaOfInterest(
#             west_lon_degree=extent[0],
#             south_lat_degree=extent[1],
#             east_lon_degree=extent[2],
#             north_lat_degree=extent[3],
#         ),
#     )
#     return CRS.from_epsg(utm_crs_list[0].code)

def createPlantPoleFeature(in_dict, geom, farm, field, subfield, row):
    """Create a QGIS feature from a dictionary with keys

    Args:
        in_dict (dict): A dictionary with the following fields
        geom (obj): a QGIS Point geometry
        farm (int): The farm ID
        field (int): The field ID
        subfield (int): The subfield ID
        row (int): The row ID

    Returns:
        obj: A QGIS feature object
    """
    if not in_dict["label"]:
        label = None
    else:
        label = str(in_dict["label"])
    if not in_dict["theory"]:
        theory = None
    else:
        theory = in_dict["theory"]
    if not in_dict["evidence"]:
        evidence = None
    else:
        evidence = in_dict["evidence"]
    # check if is a foro F/f or empty E/e
    if in_dict["material"] in ["F", "f", "E", "e"]:
        material = None
    # check if is a pole P/p
    elif in_dict["material"] in ["", "p", "P"]:
        raise ValueError("The first plant seem to be a pole")
    else:
        material = in_dict["material"]
    plant_feat = QgsFeature()
    plant_feat.setGeometry(geom)
    plant_feat.setAttributes([
        "plant",
        material,
        label,
        theory,
        evidence,
        farm,
        field,
        subfield,
        row
    ])
    return plant_feat