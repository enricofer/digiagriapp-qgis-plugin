<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT" sourcelanguage="en_GB">
<context>
    <name>@default</name>
    <message>
        <location filename="../test/test_translations.py" line="48"/>
        <source>Good morning</source>
        <translation>Buongiorno</translation>
    </message>
</context>
<context>
    <name>DigiAgriApp</name>
    <message>
        <location filename="../digiagriapp.py" line="2033"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="137"/>
        <source>Problem to get fields for farm with ID {self.farm_id}; error: {reason}</source>
        <translation>Problema per ottenere i campi dall&apos;azienda con ID {self.farm_id}; error: {reason}</translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="2051"/>
        <source>Warning</source>
        <translation>Problema</translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1911"/>
        <source>Problem to get subfield for farm with ID {self.farm_id} with subfield with ID {fieldid}; error: {reason}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="213"/>
        <source>Problem qetting the farms: {err}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="224"/>
        <source>Problem to get rows; error: {reason}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="293"/>
        <source>Problem qetting the farms: {er}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="304"/>
        <source>Problem to get farms; error: {reason}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="388"/>
        <source>Please set {} values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="407"/>
        <source>Problem requesting the token: {er}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1726"/>
        <source>Success</source>
        <translation>Successo</translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="416"/>
        <source>Token acquired correctly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="422"/>
        <source>Problem to get the token, please check server, user and password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="438"/>
        <source>Impossible get the server url, please check the settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="2033"/>
        <source>Impossible connect to the server, please check the settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="499"/>
        <source>Layer &apos;{layer_name}&apos; imported correctly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="506"/>
        <source>Layer &apos;{layer_name}&apos; not imported reason: {reason}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="691"/>
        <source>Problem to get plants for farm with ID {self.farm_id} with field with ID {fieldid}; error: {reason}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1343"/>
        <source>Import</source>
        <translation>Importazione</translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1557"/>
        <source>Vector</source>
        <translation>Vettoriale</translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1356"/>
        <source>Create plants from CSV file</source>
        <translation>Crea piante da file CSV</translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1560"/>
        <source>Raster</source>
        <translation>Raster</translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1376"/>
        <source>Export</source>
        <translation>Esportazione</translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1401"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1412"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1497"/>
        <source>DigiAgriApp</source>
        <translation>DigiAgriApp</translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1523"/>
        <source>&amp;DigiAgriApp</source>
        <translation>&amp;DigiAgriApp</translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="2011"/>
        <source>&lt;b&gt;Selected farm is {self.farm}&lt;/b&gt;</source>
        <translation>&lt;b&gt;L&apos;azienda selezionata è {self.farm}&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1604"/>
        <source>field</source>
        <translation>campo</translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1604"/>
        <source>subfield</source>
        <translation>appezzamento</translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1604"/>
        <source>row</source>
        <translation>filare</translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1604"/>
        <source>plant</source>
        <translation>pianta</translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1693"/>
        <source>Raster not yet implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1726"/>
        <source>Layer &apos;{layer.name()}&apos; correctly written to {file_output}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1733"/>
        <source>Layer &apos;{layer.name()}&apos; not written correctly: {error}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1761"/>
        <source>Please set the input CSV file path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1768"/>
        <source>The input CSV file path {file_in} does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1776"/>
        <source>Please set the field ID selecting a field</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1784"/>
        <source>Please set the subfield ID selecting a subfield</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="1800"/>
        <source>CSV separator not recognized, please use one between &apos;|&apos;, &apos;;&apos;, &apos;,&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="2017"/>
        <source>Please set the farm in the settings window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="2025"/>
        <source>Please set the output file path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="2051"/>
        <source>There are no fields in the server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp.py" line="760"/>
        <source>Problem to get poles for farm with ID {self.farm_id} with field with ID {fieldid}; error: {reason}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DigiAgriAppAbout</name>
    <message>
        <location filename="../ui_digiagriapp_dialog_about.py" line="86"/>
        <source>DigiAgriApp</source>
        <translation>DigiAgriApp</translation>
    </message>
    <message>
        <location filename="../ui_digiagriapp_dialog_about.py" line="85"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:/plugins/digiagriapp/icons/icon.svg&quot; width=&quot;100&quot;/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:/plugins/digiagriapp/icons/icon.svg&quot; width=&quot;100&quot;/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../ui_digiagriapp_dialog_about.py" line="88"/>
        <source>Info</source>
        <translation>Informazioni</translation>
    </message>
    <message>
        <location filename="../ui_digiagriapp_dialog_about.py" line="87"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;DigiAgriApp&lt;/span&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt; is a software solution aimed at farmers and anyone with cultivated land. &lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;DigiAgriApp&lt;/span&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt; is able to save operations carried out in the field, data from different devices (mobile, weather stations, remote such as drones or satellites).&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;DigiAgriApp&lt;/span&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt; consists of a server that can be connected to via several clients, the main one being a cross-platform mobile application, then there is this QGIS plugin for uploading geographical data, and finally there is a web interface to the database for system administrators.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;DigiAgriApp&lt;/span&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt; è una soluzione software pensata per gli agricoltori e chiunque abbia della terra da coltivare. &lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;DigiAgriApp&lt;/span&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt; è in grado di salvare operazioni effettuate in campo, dati da diverse sorgenti (cellulari, stazioni meteo, remote come droni o satelliti).&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;DigiAgriApp&lt;/span&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt; consiste di un server che può essere connesso attraverso diversi clients, il principale è un&apos;applicazione multi piattaforma incluso dispositivi mobili, un plugin per QGIS per la gestione dei dati geografici, e infine c&apos;è un&apos;interfaccia web al database per i manager dei dati.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../ui_digiagriapp_dialog_about.py" line="90"/>
        <source>License</source>
        <translation>Licenza</translation>
    </message>
    <message>
        <location filename="../ui_digiagriapp_dialog_about.py" line="89"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;GNU GENERAL PUBLIC LICENSE&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Version 3, 29 June 2007&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;More info at &lt;/span&gt;&lt;a href=&quot;https://www.gnu.org/licenses/quick-guide-gplv3.html&quot;&gt;&lt;span style=&quot; font-size:14pt; text-decoration: underline; color:#0000ff;&quot;&gt;Free Software Foundation website&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;GNU GENERAL PUBLIC LICENSE&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Versione 3, 29 Giugno 2007&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Maggiori informazioni sul sito della &lt;/span&gt;&lt;a href=&quot;https://www.gnu.org/licenses/quick-guide-gplv3.html&quot;&gt;&lt;span style=&quot; font-size:14pt; text-decoration: underline; color:#0000ff;&quot;&gt;Free Software Foundation&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../ui_digiagriapp_dialog_about.py" line="91"/>
        <source>Contributors</source>
        <translation>Contributori</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_about_ui.py" line="107"/>
        <source>with the contribution of &lt;a href=&quot;https://www.fondazionevrt.it/&quot; target=&quot;_blank&quot;&gt;&lt;b&gt;Fondazione VRT&lt;/b&gt;&lt;/a&gt;</source>
        <translation>con il contributo della &lt;a href=&quot;https://www.fondazionevrt.it/&quot; target=&quot;_blank&quot;&gt;&lt;b&gt;Fondazione VRT&lt;/b&gt;&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_about_ui.py" line="108"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:/plugins/digiagriapp/icons/vrt-logo.png&quot; width=&quot;150&quot;/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_about_ui.py" line="109"/>
        <source>with the support of&lt;br&gt; &lt;a href=&quot;https://www.fmach.it&quot; target=&quot;_blank&quot;&gt;&lt;b&gt;Fondazione Edmund Mach&lt;/b&gt;&lt;/a&gt;</source>
        <translation>con il supporto della &lt;a href=&quot;https://www.fmach.it&quot; target=&quot;_blank&quot;&gt;&lt;b&gt;Fondazione Edmund Mach&lt;/b&gt;&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_about_ui.py" line="110"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:/plugins/digiagriapp/icons/fem-logo.png&quot; width=&quot;200&quot;/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DigiAgriAppDialogBase</name>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="303"/>
        <source>DigiAgriApp</source>
        <translation>DigiAgriApp</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="46"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="56"/>
        <source>Server:</source>
        <translation>Server:</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="70"/>
        <source>Username:</source>
        <translation>Nome utente:</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="84"/>
        <source>Password:</source>
        <translation>Password:</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="102"/>
        <source>Available farms:</source>
        <translation>Aziende disponibili:</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="116"/>
        <source>Save credential</source>
        <translation>Salva credenziali</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="130"/>
        <source>Import</source>
        <translation>Importazione</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="137"/>
        <source>Layer name</source>
        <translation>Nome del layer</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="148"/>
        <source>DigiAgriApp type</source>
        <translation>Tipologia in DigiAgriApp</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="159"/>
        <source>All features</source>
        <translation>Tutti gli elementi</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="229"/>
        <source>Upload</source>
        <translation>Carica</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="185"/>
        <source>Layer Name</source>
        <translation>Nome del layer</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="196"/>
        <source>Time</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="207"/>
        <source>Field</source>
        <translation>Campo</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="218"/>
        <source>Sensor</source>
        <translation>Sensore</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="244"/>
        <source>Export</source>
        <translation>Esportazione</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="250"/>
        <source>Export field for selected farm</source>
        <translation>Esporta campi per l&apos;azienda selezionata</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="259"/>
        <source>Select field</source>
        <translation>Seleziona campo</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_base.ui" line="285"/>
        <source>Help</source>
        <translation>Aiuto</translation>
    </message>
</context>
<context>
    <name>DigiAgriAppDialogImport</name>
    <message>
        <location filename="../digiagriapp_dialog_import.ui" line="57"/>
        <source>DigiAgriApp</source>
        <translation>DigiAgriApp</translation>
    </message>
    <message>
        <location filename="../ui_digiagriapp_dialog_export.py" line="85"/>
        <source>Select a field to download sub elements</source>
        <translation>Seleziona un campo per scaricare i sotto elementi</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_import.ui" line="28"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:/plugins/digiagriapp/icons/icon.svg&quot; height=&quot;100&quot;/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:/plugins/digiagriapp/icons/icon.svg&quot; height=&quot;100&quot;/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../ui_digiagriapp_dialog_export.py" line="87"/>
        <source>Download fields for farm {}</source>
        <translation>Scarica campi per azienda {}</translation>
    </message>
    <message>
        <location filename="../ui_digiagriapp_dialog_export.py" line="88"/>
        <source>Download rows for field</source>
        <translation>Scarica filari per campo</translation>
    </message>
    <message>
        <location filename="../ui_digiagriapp_dialog_export.py" line="90"/>
        <source>Download subfields for field</source>
        <translation>Scarica appezzamento per campo</translation>
    </message>
    <message>
        <location filename="../ui_digiagriapp_dialog_export.py" line="91"/>
        <source>Download plants for field</source>
        <translation>Scarica piante per campo</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_import.ui" line="73"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui_digiagriapp_dialog_export_vector.py" line="85"/>
        <source>Download fields for farm</source>
        <translation>Scarica campi per azienda</translation>
    </message>
</context>
<context>
    <name>DigiAgriAppDialogPlants</name>
    <message>
        <location filename="../digiagriapp_dialog_plants.ui" line="138"/>
        <source>DigiAgriApp</source>
        <translation>DigiAgriApp</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_plants.ui" line="60"/>
        <source>Select CSV file</source>
        <translation>Seleziona file CSV</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_plants.ui" line="70"/>
        <source>Select row (optional)</source>
        <translation>Seleziona filare (opzionale)</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_plants.ui" line="83"/>
        <source>Select field</source>
        <translation>Seleziona campo</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_plants.ui" line="90"/>
        <source>Select subfield (optional)</source>
        <translation>Seleziona appezzamento (opzionale)</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_plants.ui" line="159"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:/plugins/digiagriapp/icons/icon.svg&quot; height=&quot;100&quot;/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:/plugins/digiagriapp/icons/icon.svg&quot; height=&quot;100&quot;/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_plants_ui.py" line="108"/>
        <source>Create plant for a row from a CSV file. Using the geometry of the selected row it create the plants pushing them inside the row geometry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_plants.ui" line="39"/>
        <source>Check material information like spacing, double plant in a hole, etc</source>
        <translation>Controlla le informazioni sul materiale tipo distanza, doppia pianta in buco, ecc</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_plants.ui" line="99"/>
        <source>Keep the pole at the same distance in all rows</source>
        <translation>Tiene il palo al stessa distanza in tutti i filari</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_plants.ui" line="174"/>
        <source>Create plant for one or more rows from a CSV file. Using the geometry of the selected row it create the plants pushing them inside the row geometry</source>
        <translation>Crea piante per uno o più filari da un file CSV. Utilizzando la geometria della riga selezionata, crea le piante adattandole all&apos;interno della geometria della riga</translation>
    </message>
</context>
<context>
    <name>DigiAgriAppExportVector</name>
    <message>
        <location filename="../digiagriapp_dialog_export_vector.ui" line="177"/>
        <source>DigiAgriApp</source>
        <translation>DigiAgriApp</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_export_vector.ui" line="138"/>
        <source>Download subfields for all fields</source>
        <translation>Scarica appezzamenti per tutti i campi</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_export_vector_ui.py" line="102"/>
        <source>Download plants for all field</source>
        <translation>Scarica piante per tutto il campo</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_export_vector.ui" line="63"/>
        <source>Download fields for farm</source>
        <translation>Scarica campi per l&apos;azienda</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_export_vector.ui" line="84"/>
        <source>Select output file</source>
        <translation>Seleziona il file di output</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_export_vector.ui" line="152"/>
        <source>Download selected features from DigiAgriApp server and save them into Geopackage file.</source>
        <translation>Scarica gli elementi seleziona del server DigiAgriApp and salva in un file Geopackage.</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_export_vector.ui" line="28"/>
        <source>Download rows for all fields</source>
        <translation>Scarica filari per tutti i campi</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_export_vector.ui" line="42"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:/plugins/digiagriapp/icons/icon.svg&quot; height=&quot;100&quot;/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:/plugins/digiagriapp/icons/icon.svg&quot; height=&quot;100&quot;/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_export_vector.ui" line="98"/>
        <source>Download plants for all fields</source>
        <translation>Scarica le piante per tutti i campi</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_export_vector.ui" line="113"/>
        <source>Download poles for all fields</source>
        <translation>Scarica i pali per tutti i campi</translation>
    </message>
</context>
<context>
    <name>DigiAgriAppSettings</name>
    <message>
        <location filename="../digiagriapp_dialog_settings.ui" line="52"/>
        <source>DigiAgriApp</source>
        <translation>DigiAgriApp</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_settings.ui" line="68"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set up the connection with the &lt;span style=&quot; font-weight:600;&quot;&gt;DigiAgriApp&lt;/span&gt; server.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Impostare la connessione con il server &lt;span style=&quot; font-weight:600;&quot;&gt;DigiAgriApp&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_settings.ui" line="79"/>
        <source>Server:</source>
        <translation>Server:</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_settings.ui" line="93"/>
        <source>Username:</source>
        <translation>Nome utente:</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_settings.ui" line="107"/>
        <source>Password:</source>
        <translation>Password:</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_settings.ui" line="125"/>
        <source>Available farms:</source>
        <translation>Aziende disponibili:</translation>
    </message>
    <message>
        <location filename="../ui_digiagriapp_dialog_settings.py" line="108"/>
        <source>Save credential</source>
        <translation>Salva credenziali</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_settings.ui" line="176"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:/plugins/digiagriapp/icons/icon.svg&quot; width=&quot;100&quot;/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:/plugins/digiagriapp/icons/icon.svg&quot; width=&quot;100&quot;/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_settings.ui" line="26"/>
        <source>Close</source>
        <translation>Chiudere</translation>
    </message>
    <message>
        <location filename="../digiagriapp_dialog_settings.ui" line="148"/>
        <source>Save</source>
        <translation>Salvare</translation>
    </message>
</context>
</TS>
